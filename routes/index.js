var express = require('express');
var router = express.Router();
const nodemailer = require("nodemailer");
const messages = require("../Model/message");
const cource = require("../Model/cource");
const addmissions = require("../Model/user_addmission");
const commonFunction = require("./commonFunction");
const adminAuth = require("../config/admin");
var jwt = require('jsonwebtoken');
const multer = require("multer");
const path = require("path");
const { response } = require('express');


// set storage engine
var storage = multer.diskStorage({
  destination: './public/uploads/',
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }

});

//INit upload
const upload = multer({
  storage: storage,
  limits: { fileSize: 3000000 },
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  }
}).single('myImage');

// check File Type
function checkFileType(file, cb) {
  //allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  //check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  //check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);

  } else {
    cb('Error : Images Only');
  }
}

router.post('/upload', async (req, res) => {
  courseId = req.query.courseId;
  console.log("courceId-3->", courseId);
  const response = await cource.findOne({ _id: courseId });
  upload(req, res, async (err) => {
    if (err) {
      res.render('admin/image_upload', { response: response, msg: err })
    } else {
      if (req.file == undefined) {
        res.render('admin/image_upload', { response: response, msg: 'Error: No File Selected!' });
      } else {
        console.log(req.file);
        const updateResponse = await cource.updateOne({ _id: courseId }, { $set: { image: req.file.filename } });
        const response = await cource.findOne({ _id: courseId });
        console.log("updateResponse-->", updateResponse)
        res.render('admin/image_upload', {
          response: response, msg: 'File Uploaded!', file: `uploads/${req.file.filename}`
        });
        console.log('final file-->', req.file.filename)
      }
    }
  });
});



router.get('/upload', async function (req, res, next) {

  const token = req.cookies.s2sadminToken;
  if (token) {
    jwt.verify(token, 'secret', async (err, decoded) => {
      if (!err) {
        courseId = req.query.courseId;
        console.log("courceId-->", courseId);
        await cource.findOne({ _id: courseId }, function (err, response) {
          if (err) {
            console.log("Error Occured-->", err)
          } else {
            if (response) {
              console.log("response-->", response)
              res.render('admin/image_upload', { response: response });
            } else {
              console.log("course not found")
            }
          }
        });
        console.log(decoded);
      } else res.render("admin/login");
    });
  } else res.render("admin/login");


});

router.post('/delete', function (req, res, next) {

  const token = req.cookies.s2sadminToken;
  if (token) {
    jwt.verify(token, 'secret', async (err, decoded) => {
      if (!err) {
        var courseId = req.body.courceId;
        console.log("courceId-->", courseId);
        const response = await cource.findOneAndDelete({ _id: courseId });
        if (response) {
          console.log("response-->", response)
          res.send({ success: "DELETED!" });
        } else {
          res.send({ error: "course not found" })
          console.log("cource not found")
        }
        console.log(decoded);
      } else res.render("admin/login");
    });
  } else res.render("admin/login");
});

router.get('/error', function (req, res, next) {

  res.render("error", {
    message: 'CAN NOT LOAD PAGE PROPERLY/CAN NOT CONNECT TO DATABASE',
  });
});

router.get('/test', function (req, res, next) {

  res.render("test_2", {
  });
});



router.get('/about', function (req, res, next) {

  res.render("about", {
    title: "About"
  });
});



router.post('/add-cource', function (req, res, next) {

  var crs_title = req.body.title;
  // var content = req.body.content;
  var str = req.body.content;
  var content = str.split("; ");
  console.log("content-->", content);
  var crs_type = req.body.type;
  var display_area = req.body.d_area;
  var long_des = req.body.long_description;
  var short_des = req.body.short_description;
  var requir = req.body.requirement;
  var requirement = requir.split("; ");
  console.log("requirement-->", requirement);
  var crs_fee = req.body.fee;
  var duration = req.body.duration;
  var data = {
    crs_title: crs_title,
    crs_type: crs_type,
    d_area: display_area,
    short_des: short_des,
    long_des: long_des,
    content: content,
    requriment: requirement,
    crs_fee: crs_fee,
    duration: duration
  }
  console.log("data-->", data);

  cource.create(data, function (err, created) {
    if (err) {
      console.log(err);
      res.send({ error: "opps! error occured" })
    } else {
      if (created) {
        console.log("created successfully-->", created);
        res.send({ success: "cource added" })
      } else {
        console.log("error occured")
        res.send({ error: "error occured" })
      }
    }

  });

});





/* GET home page. */
router.get('/', function (req, res, next) {
  cource.find({}, function (err, response) {
    console.log(response);
    res.render("index", { response: response, title: 'StepToSoft' });
  })

});

router.get('/admin', function (req, res, next) {
  const token = req.cookies.s2sadminToken;
  if (token) {
    jwt.verify(token, 'secret', (err, decoded) => {
      if (!err) {
        res.render("admin/dashbord");
        console.log(decoded);
      } else res.render("admin/login");
    });
  } else res.render("admin/login");
});

router.get('/admin-dashboard', function (req, res, next) {
  const token = req.cookies.s2sadminToken;
  if (token) {
    jwt.verify(token, 'secret', (err, decoded) => {
      if (!err) {
        res.render("admin/dashbord");
        console.log(decoded);
      } else res.render("admin/login");
    });
  } else res.render("admin/login");
});


router.get('/mail', function (req, res, next) {
  const token = req.cookies.s2sadminToken;
  if (token) {
    jwt.verify(token, 'secret', async (err, decoded) => {
      if (!err) {
        await messages.find({}, function (err, mails) {
          res.render("admin/mails", { mails: mails });
          console.log("mails-->", mails);
        });

      } else res.render("admin/login");
    });
  } else res.render("admin/login");
});

router.get('/edit-cources', function (req, res, next) {
  const token = req.cookies.s2sadminToken;
  if (token) {
    jwt.verify(token, 'secret', (err, decoded) => {
      if (!err) {
        cource.find({}, function (err, response) {
          console.log(response);
          res.render("admin/cources", { response: response });
        })
        console.log(decoded);
      } else res.render("admin/login");
    });
  } else res.render("admin/login");
});

router.get('/students', function (req, res, next) {
  const token = req.cookies.s2sadminToken;
  if (token) {
    jwt.verify(token, 'secret', (err, decoded) => {
      if (!err) {
        res.render('admin/students', { title: 'StepToSoft' });
        console.log(decoded);
      } else res.render("admin/login");
    });
  } else res.render("admin/login");

});


router.get('/cource', async function (req, res, next) {
  courseId = req.query.courseId;
  console.log("courceId-->", courseId);
  await cource.findOne({ _id: courseId }, function (err, response) {
    if (err) {
      console.log("err occured-->", err)
    } else {
      if (response) {
        console.log("response-->", response)
        res.render('cource_details', { data: response });
      } else {
        console.log("course not found")
      }
    }
  });

});

// router.get('/add-cource', function (req, res, next) {
//   const token = req.cookies.s2sadminToken;
//   if (token) {
//     jwt.verify(token, 'secret', (err, decoded) => {
//       if (!err) {
//         res.render('admin/add_cource', { title: 'Cources' });
//         console.log(decoded);
//       } else res.render("admin/login");
//     });
//   } else res.render("admin/login");

// });



router.post('/send_mail', async function (req, res, next) {
  console.log("ajax called working")
  var name = req.body.name;
  var email = req.body.email;
  var phone = req.body.phone;
  var message = req.body.message;
  var data = {
    name: name,
    email: email,
    phone: phone,
    message: message
  }
  console.log(data);
  await messages.create(data, function (err, created) {
    if (err) {
      console.log("can not save user info", err);
    } else {
      if (created) {
        console.log("saved to db");
      } else {
        console.log("can not save to db");
      }
    }
  });
  if (email) {
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: "anurag.developer3000@gmail.com",
        pass: "developer@3000",
      },
    });
    // send mail with defined transport object
    await transporter.sendMail({
      from: 'anurag.developer3000@gmail.com',   // sender address
      to: email, // list of receivers
      subject: "Subject......", // Subject line
      text: "Thankyou For Mailing", // plain text body
      html: '<a href="www.steptosoft.com">Visit</a>', // html content
    }, function (err, mailSent) {
      if (err) {
        console.log("err", err);
        res.send({ "Error": "Email Does not Exist" })
      } else {
        if (mailSent) {
          console.log("mail sent successful")
          console.log("mailSent", mailSent)
          res.send({ success: "Thankyou For Contacting Us" })
        }
        else {
          console.log("mail not sent")
          res.send({ error: "mail does not exist" })
        }

      }
    });
  } else {
    res.send({ error: "Something went wrong" });
  }


});

router.post('/addmission', async function (req, res, next) {
  console.log("ajax called working")
  var name = req.body.name;
  var email = req.body.email;
  var phone = req.body.phone;
  var subject = "addmission";
  var html = "<h1>Thankyou</h1>"
  var data = {
    name: name,
    email: email,
    phone: phone,
  }
  console.log(data);
  addmissions.create(data, function (err, created) {
    if (err) {
      console.log("can not save user info", err);
    } else {
      if (created) {
        console.log("saved to db");
        console.log(created);
      } else {
        console.log("can not save to db");
      }
    }
  });
  if (email) {
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: "anurag.developer3000@gmail.com",
        pass: "developer@3000",
      },
    });
    // send mail with defined transport object
    await transporter.sendMail({
      from: 'anurag.developer3000@gmail.com',   // sender address
      to: email, // list of receivers
      subject: "Subject......", // Subject line
      text: "Thankyou For Mailing", // plain text body
      html: '<a href="www.steptosoft.com">Visit</a>', // html content
    }, function (err, mailSent) {
      if (err) {
        console.log("err", err);
        res.send({ "Error": "Email Does not Exist" })
      } else {
        if (mailSent) {
          console.log("mail sent successful")
          console.log("mailSent", mailSent)
          res.send({ success: "Thankyou For Contacting Us" })
        }
        else {
          console.log("mail not sent")
          res.send({ error: "mail does not exist" })
        }

      }
    });
  } else {
    res.send({ error: "Something went wrong" });
  }

});


router.post('/admin-login', function (req, res, next) {

  var userName = req.body.username;
  var password = req.body.password;
  console.log("recived user-->", userName);
  console.log("admin user-->", adminAuth.userName);
  console.log("admin pass-->", adminAuth.password);
  if (userName == adminAuth.userName && password == adminAuth.password) {
    jwt.sign({
      email: userName,
      userId: password,
    }, 'secret', (err, jwtdata) => {
      if (jwtdata) {
        res.send({ jwtdata });
        console.log("jwtdata--->", jwtdata);
        console.log("logged in")
      }
      else {
        console.log("error")
        res.send({ error: "something went wrong please try again" })
      }
    });
  }
  else {
    console.log("user/password is wrong")
    res.send({ error: "user/password is wrong" })
  }
});
module.exports = router;
