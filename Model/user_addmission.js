const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;

const addmissionSchema = new Schema({

    name: { type: String, max: 100, required: true },
    email: { type: String, max: 100, default: null, required: true, },
    phone: { type: String, default: null, required: true },
    SentOn: { type: Date, default: Date.now() },
    seen: { type: String, default: "No" },
    Replyed: { type: String, default: "No" },
    SentByIp: { type: String, max: 30 },
    ReplyedOn: { type: Date, default: null },
    ReplyedByIp: { type: String, max: 30 },
});

module.exports = mongoose.model("addmission", addmissionSchema);
