const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;

const courceSchema = new Schema({

    crs_title: { type: String, max: 100, required: true },
    crs_type: { type: String, max: 100, required: true },
    d_area: { type: String },
    short_des: { type: String, default: null, required: true, },
    long_des: { type: String, default: null, required: true },
    content: { type: Array, default: "null" },
    requriment: { type: Array, default: "null" },
    crs_fee: { type: String, max: 100, required: true },
    duration: { type: String },
    image: { type: String, default: null, },
    active: { type: String, default: "No" },
    createdByIp: { type: String, max: 30 },
    createdOn: { type: Date, default: null },
});

module.exports = mongoose.model("cource", courceSchema);
