// $(document).on("click", "#add_btn", function () {
//     var content = "";
//     var str = $("#content").val();
//     var res = str.split(",");
//     $("#cntnt").val(res);
//     $("#content").hide();
//     $("#add_btn").hide();
//     $(".cnt_ins").hide();
//     console.log(res);
//     for (i = 0; i < res.length; i++) {
//         content += i + 1 + " " + res[i] + "<br>";
//     }
//     $("#cntnt").append(content);
//     console.log(content);
// })
$(document).on("click", "#add_crs", function () {
    var content = $("#content").val();
    var title = $("#crs_title").val();
    var type = $("#crs_type").val();
    var d_area = $('input[name="optradio"]:checked').val();
    var long_description = $("#crs_desc").val();
    var short_description = $("#crs_shr_desc").val();
    var requirement = $("#req").val();
    var fee = $("#fee").val();
    var duration = $("#dwd").val();

    var data = {
        title: title,
        content: content,
        type: type,
        d_area: d_area,
        long_description: long_description,
        short_description: short_description,
        requirement: requirement,
        fee: fee,
        duration: duration
    }
    console.log(data);
    $.ajax({
        url: "/add-cource",
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function (res) {
            if (res.success) {
                alert(res.success);
                $("input[type=text], textarea").val("");
                location.reload();
            } else if (res.error) {
                alert(res.error);
            }
        },
    });
});

$(document).on("click", "#delete", function () {
    var responce = confirm("Confirm Delete!");
    console.log("result-->", responce)
    if (responce) {
        var courceId = $(this).closest("#delete").val();
        console.log("courceId-->", courceId);
        data = {
            courceId: courceId,
        }
        console.log("data-->", data)
        $.ajax({
            url: "/delete",
            type: "POST",
            data: data,
            success: function (res) {
                if (res.success) {
                    alert(res.success);
                    location.reload();

                } else {
                    alert("something went wrong!!!");
                    location.reload();
                }
            }
        })
    }
});